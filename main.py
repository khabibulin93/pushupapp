from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.accordion import Accordion, AccordionItem
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.button import Button
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen, FadeTransition
from functools import partial

import sqlite3 as slite

con = slite.connect('data.db')

class MenuScreen(Screen):
    pass

class PushupScreen(Screen):
    
    def on_pre_enter(self, *args):
        self.infalls = []
        with con:
            cur = con.cursor()
            cur.execute("SELECT * FROM workout")
            while True:
                info = cur.fetchone()
                if info == None:
                    break
                self.infalls.append(info)
        if self.infall == self.infalls:
            pass
        else:
            self.ids.pushup.clear_widgets()
            self.create_widgets(self.infalls)
            

    def event (self, index, instance):
        do_pushup_screen = self.manager.get_screen('dopushup')
        setattr(do_pushup_screen, 'index', index)
        self.manager.current='dopushup'

    def __init__ (self, **kwargs):
        super(PushupScreen, self).__init__(**kwargs)
        self.infall = []
        layouts = []
        with con:
            cur = con.cursor()
            cur.execute("SELECT * FROM workout")
            while True:
                info = cur.fetchone()
                if info == None:
                    break
                self.infall.append(info)
        self.create_widgets(self.infall)
        
    def create_widgets(self, set_info):
        layouts = []
        for day, pushup, complete, repeat in set_info:
            layout = BoxLayout(orientation='horizontal', padding=[100, 0])
            layout.add_widget(Label(text='{}' .format(day), size_hint=(None, 1)))
            layout.add_widget(Button(text='{}'.format(sum([int(i) for i in pushup.split(',')])), size_hint=(.7, 1),
                              on_release=partial(self.event, pushup)))
            layout.add_widget(Label(text='{}' .format('Not' if complete == 0  else 'Done'), size_hint=(None, 1), size=[50,50]))
            layout.add_widget(Label(text='{}' .format('No' if repeat == 0  else 'Need'), size_hint=(None, 1), size=[50,50]))
            layouts.append(layout)

        for i in layouts:
            self.ids.pushup.add_widget(i)

class DoPushupScreen(Screen):

    def on_pre_enter(self, *args):
        self.pushup_prev_pos = 1
        self.pushup = [int(i) for i in self.index.split(',')]
        self.ids.prevpushup.text = '-'
        self.ids.nowpushup.text = str(self.pushup[0])
        self.ids.nextpushup.text = str(self.pushup[self.pushup_prev_pos])

    def __init__(self, **kwargs):
        super(DoPushupScreen, self).__init__(**kwargs)
    
    def next_exercise (self):
        self.ids.prevpushup.text = self.ids.nowpushup.text
        self.ids.nowpushup.text = self.ids.nextpushup.text
        self.ids.nextbutt.text = 'Next'
        self.ids.backtomenu.text = 'GiveUp'
        self.ids.backtomenu.on_release = self.giveup_exercise
        self.pushup_prev_pos += 1
        try:
            self.ids.nextpushup.text = str(self.pushup[self.pushup_prev_pos])
        except IndexError:
            self.ids.nextpushup.text = "That's all for today"
            self.ids.nextbutt.text = "Finish"
            self.ids.nextbutt.on_press = self.finish_exercise

    def finish_exercise (self):
        cur = con.cursor()
        cur.execute("UPDATE workout SET ifcomplete = '1' WHERE pushup = '%s'" % self.index)
        # Comment for testing suite, on deploy need to be uncomment
        con.commit()
        self.manager.current = 'pushuplist'

    def giveup_exercise (self):
        cur = con.cursor()
        cur.execute("UPDATE workout SET ifrepeat = '1' WHERE pushup = '%s'" % self.index)
        # Comment for testing suite, on deploy need to be uncomment
        con.commit()
        self.manager.current = 'pushuplist'

class AboutProgrammScreen(Screen):
    pass

class AboutWorkoutScreen(Screen):
    pass

class OptionsScreen(Screen):

    def reset_db(self, *args):
        popup = Popup(title='DB is reset', title_align='center', separator_height=0,
                      size_hint=(None, None), size=(250, 60), pos_hint={ 'center_x': .5, 'y': .8})
        con = slite.connect('data.db')
        f = open('datashift.sql','r', encoding='utf-8')
        sql = f.read()
        cur = con.cursor()
        cur.executescript(sql)
        popup.open()

    def __init__(self, **kwargs):
        super(OptionsScreen, self).__init__(**kwargs)
        self.ids.mainoption.add_widget(Button(text='Reset DB', on_release=self.reset_db, size=(400, 50),
         size_hint=(.5, None), pos_hint={'center_x': .5, 'y': .8}))

class CustomAccordion(Accordion):

    def __init__(self, **kwargs):
        super(CustomAccordion, self).__init__(**kwargs)
        self.orientation = 'vertical'
        self.infall = []
        items = []
        with con:
            cur = con.cursor()
            cur.execute("SELECT * FROM info")
            while True:
                info = cur.fetchone()
                if info == None:
                    break
                self.infall.append(info)

        for section, infosection in self.infall:
            item = AccordionItem(title='{}'.format(section))
            item.add_widget(Label(text='{}'.format(infosection), text_size=(500, None)))
            items.append(item)
        
        for i in items:
            self.add_widget(i)

with open("main.kv", 'r', encoding='utf8') as f:
     Builder.load_string(f.read())

sm = ScreenManager()
sm.add_widget(MenuScreen(name='menu'))
sm.add_widget(PushupScreen(name='pushuplist'))
sm.add_widget(DoPushupScreen(name='dopushup'))
sm.add_widget(OptionsScreen(name='options'))
sm.add_widget(AboutProgrammScreen(name='aboutprogramm'))
sm.add_widget(AboutWorkoutScreen(name='aboutworkout'))

class MainApp(App):
    def build(self):
        return sm

MainApp().run()